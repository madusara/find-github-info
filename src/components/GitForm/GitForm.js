import React from 'react';

const formStyles = {
  marginBottom: '70px',
}

class GitForm extends React.Component {
  state = {
    value: '',
  }

  setInputValue = ev => {
    this.setState({
      value: ev.target.value,
    });
  }

  getData = (e) => {
    e.preventDefault();
    const EnteredUsername = e.target.elements.username.value;
    if(this.state.value !== '') {
      this.props.fetchUserInfo(EnteredUsername);
    }
    this.setState({
      value: '',
    });
  }

  render() {
    return (
      <form style={formStyles} onSubmit={this.getData}>
        <input type="text" name="username" placeholder="Github Username" onChange={this.setInputValue} value={this.state.value}/>
        <button>Submit</button>
      </form>
    );
  }
}

export default GitForm;