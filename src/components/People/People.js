import React from 'react';
import './People.scss';

class People extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      people: [],
      fetchSuccessful: false,
    }
  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
    .then( res1 => res1.json())
    .then( json => { this.setState({
        people: json,
        fetchSuccessful: true,
      })
    })
    console.log(this.state.people)
  }

  render() {
    let {people, fetchSuccessful} = this.state;
    return (
      <div className="People">
        <h1>Showing some people</h1>
        <div className="table-wrap">
        {fetchSuccessful ? 
          <table className="table table-striped" style={{ textAlign: 'left' }}>
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Website</th>
              </tr>
            </thead>
            <tbody>
              {people.map( person => (
                <tr>
                  <td>{person.id}</td>
                  <td>{person.name}</td>
                  <td>{person.email}</td>
                  <td>{person.phone}</td>
                  <td>{person.website}</td>
                </tr>
              ))};
            </tbody>
          </table>
          :
          <h3>L O A D I N G</h3>
        }
        </div>
      </div>
    );
  }
}

export default People;
