import React from 'react';
import './Home.scss'

const styles = {
  padding: '30px',
  fontSize: '20px',
  textAlign: 'center',
  borderBottom: '1px dashed grey'
}

const Home = () => {
  return (
    <div style={styles}>
      <a className="navs" href="/users">People</a>
      <a className="navs" href="/gitinfo">Git Info</a>
      <a className="navs" href="/local-store">Local Store</a>
    </div>
  );
}

export default Home;
