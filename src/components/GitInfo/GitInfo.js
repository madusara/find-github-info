import React from 'react';
import './GitInfo.scss';
import GitForm from '../GitForm/GitForm';
import axios from 'axios';

class GitInfo extends React.Component {
  state = {
    spinner: false,
    dataLoaded: '',
    username: '',
    profileImageURL: '',
    repoCount: '',
    bio: '',
    profileURL: '',
    inputValue: '',
  }

  fetchUserInfo = username => {
    this.setState({
      spinner: true,
    });
    axios.get(`https://api.github.com/users/${username}`)
    .then((res) => {
      this.setState({
        username: res.data.login,
        avatarURL: res.data.avatar_url,
        repoCount: res.data.public_repos,
        bio: res.data.bio,
        profileURL: res.data.html_url,
        dataLoaded: true,
        spinner: false,
      });
    })
    .catch((err) => {
      console.log("err",err);
      console.log("err.message", err.message);
      this.setState({
        spinner: false,
        dataLoaded: false,
      });
      if(err.message === 'Network Error') document.getElementById("error-name").innerHTML = "Network Error";
    })
  }

  render() {
    if((this.state.dataLoaded === false) && (this.state.spinner === true)) {
      return (
        <div style={{ textAlign: 'center' }}>
          <h1>L O A D I N G</h1>
        </div>
      );
    } else if((this.state.dataLoaded === false) && (this.state.spinner === false)){
      return (
        <div style={{ textAlign: 'center' }}>
          <h1 id="error-name"style={{ color: '#f00000'}}>User not found</h1>
          <GitForm getData={this.getData} fetchUserInfo={this.fetchUserInfo}/>
        </div>
      );
    } else if((this.state.dataLoaded === true) && (this.state.spinner === false)) {
      return (
        <div className="People">
          <h1>Retrieved Github Profile</h1>
          <GitForm getData={this.getData} fetchUserInfo={this.fetchUserInfo}/>
          {this.state.dataLoaded &&
            <div className="container-fluid">
              <div className="row justify-content-around git-card">
                <div className="col git-card-image">
                  <img src={this.state.avatarURL} alt="dp" />
                </div>
                <div className="col git-card-details">
                  <div className="username">{this.state.username}</div>
                  <div className="bio">{this.state.bio}</div>
                  <div className="repo-count">{this.state.repoCount} Public Repositories</div>
                  <div className="profile-url">URL: <a href={this.state.profileURL}>{this.state.profileURL}</a></div>
                </div>
              </div>
            </div>
          }
        </div>
      );
    } else {
      return (
        <div className="People">
          <h1>Retrieve Github Profile</h1>
          <GitForm getData={this.getData} fetchUserInfo={this.fetchUserInfo}/>
        </div>
      );
    }
  }
}

export default GitInfo;
