import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import People from './components/People/People';
import GitInfo from './components/GitInfo/GitInfo';
import Home from './components/Home/Home';
import LocalStore from './components/LocalStorage/LocalStorage';

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Home />
        <Switch>
          <Route path="/users" component={People} />
          <Route path="/gitinfo" component={GitInfo} />
          <Route path="/local-store" component={LocalStore} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
